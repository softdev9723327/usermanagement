/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author asus
 */
public class Userservice {
    private ArrayList<User> userList;
    private int lastId = 1;

    public Userservice() {
        userList = new ArrayList<User>();
    }
    
    public User addUser(User newUser){
        newUser.setID(lastId++);
        userList.add(newUser);
        return newUser;
    }
    
    public User getUser(int index){
        return userList.get(index);
    }
    
    public ArrayList<User> getUsers(){
        return userList;
    }
    
    public int getSize(){
        return userList.size();
    }
    
    public void loginUserList(){
        for(User u:userList){
            System.out.println(u);
        }
    }
}
